"""Tests for cli utility."""

import subprocess
import sys

from ellclockin import VERSION


def subprocess_ellclockin(
    *args, stdout: bool = True, stderr: bool = False
) -> subprocess.Popen:
    """Run foo as subprocess and return subprocess handle for it."""
    return subprocess.Popen(
        [sys.executable, "-m", "ellclockin", *args],
        env={"PYTHONPATH": ":".join(sys.path)},
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE if stdout else None,
        stderr=subprocess.PIPE if stderr else None,
    )


def run_ellclockin(*args, stdout: bool = True, stderr: bool = False):
    process = subprocess_ellclockin(*args, stdout=stdout, stderr=stderr)
    process.wait()  # No need for timeout because the whole test is under timeout
    return (
        None if process.stdout is None else process.stdout.read(),
        None if process.stderr is None else process.stderr.read(),
    )


def test_version():
    """Check that we can feed data from stdin."""
    stdout, _ = run_ellclockin("--version")
    assert stdout.decode() == f"ellclockin {VERSION}\n"
