import dataclasses
import pathlib

import pytest
from shv import RpcLogin, RpcLoginType, RpcUrl, SimpleClient

from ellclockin.server import Server, ServerConfig


@pytest.fixture(name="server_config")
def fixture_server_config(tmp_path):
    config = ServerConfig.load(pathlib.Path(__file__).parent / "ellclockin-server.ini")
    config.dbfile = tmp_path / "ellclockin.db"
    return config


@pytest.fixture(name="server")
async def fixture_server(server_config):
    server = Server(server_config)
    await server.start_serving()
    yield server
    await server.terminate()


@pytest.fixture(name="url")
def fixture_url(server_config):
    """Provide RpcUrl for connecting to the server but without correct login."""
    return RpcUrl(location="localhost", port=server_config.port)


@pytest.fixture(name="url_franta")
def fixture_url_franta(url, server_config):
    """Provide RpcUrl for connecting to the server as franta."""
    return dataclasses.replace(
        url, login=RpcLogin("franta", server_config.users["franta"], RpcLoginType.SHA1)
    )


@pytest.fixture(name="url_lojza")
def fixture_url_lojza(url, server_config):
    """Provide RpcUrl for connecting to the server as lojza."""
    return dataclasses.replace(
        url, login=RpcLogin("lojza", server_config.users["lojza"], RpcLoginType.SHA1)
    )


@pytest.fixture(name="franta")
async def fixture_franta(server, url_franta):
    client = await SimpleClient.connect(url_franta)
    yield client
    await client.disconnect()


@pytest.fixture(name="lojza")
async def fixture_lojza(server, url_lojza):
    client = await SimpleClient.connect(url_lojza)
    yield client
    await client.disconnect()
