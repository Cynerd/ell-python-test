import freezegun
import pytest

from ellclockin.server.database import ServerDatabase


@pytest.fixture(name="db")
def fixture_db(server_config):
    return ServerDatabase(server_config)


def test_new_project(db):
    assert set(db.projects()) == set()
    db.new_project("test")
    db.new_project("deploy")
    assert set(db.projects()) == {"test", "deploy"}


def test_new_project_dup(db):
    db.new_project("test")
    with pytest.raises(ValueError):
        db.new_project("test")


def test_complete_project(db):
    db.new_project("test")
    db.new_project("deploy")
    assert set(db.projects_in_progress()) == {"test", "deploy"}
    db.complete_project("test")
    assert set(db.projects_in_progress()) == {"deploy"}


def test_complete_project_invalid(db):
    with pytest.raises(ValueError):
        db.complete_project("test")


def test_new_worker(db):
    assert set(db.workers()) == set()
    db.new_worker("franta")
    db.new_worker("lojza")
    assert set(db.workers()) == {"franta", "lojza"}


def test_new_worker_dup(db):
    db.new_worker("franta")
    with pytest.raises(ValueError):
        db.new_worker("franta")


def test_work(db):
    db.new_worker("franta")
    db.new_project("test")
    assert db.current_project("franta") is None
    assert db.project_workers("test") == set()
    assert db.project_seconds("test") == 0
    with freezegun.freeze_time() as frozen_datetime:
        db.work("franta", "test")
        assert db.current_project("franta") == "test"
        assert db.current_seconds("franta") == 0
        assert db.project_workers("test") == {"franta"}
        assert db.project_seconds("test") == 0
        frozen_datetime.tick(153)
        assert db.current_seconds("franta") == 153
        assert db.project_workers("test") == {"franta"}
        assert db.project_seconds("test") == 153
        db.work("franta", None)
        assert db.current_project("franta") is None
    assert db.project_workers("test") == {"franta"}
    assert db.project_seconds("test") == 153


def test_add_work(db):
    db.new_worker("franta")
    db.new_project("test")
    db.add_work("franta", "test", 500)
    db.add_work("franta", "test", 300)
    assert db.project_complete("test") is False
    assert db.project_workers("test") == {"franta"}
    assert db.project_seconds("test") == 800
