"""Elektroline clock-in tool."""

from .__version__ import VERSION

__all__ = [
    "VERSION",
]
