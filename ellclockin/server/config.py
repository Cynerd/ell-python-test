"""Configuration for the server."""

from __future__ import annotations

import configparser
import dataclasses
import pathlib

import shv
import shv.broker


@dataclasses.dataclass
class ServerConfig:
    """Elldev server config."""

    port: int = 3755
    """TCP/IP port where server will be listening."""

    users: dict[str, str] = dataclasses.field(default_factory=dict)
    """Mapping of user name to their passwords."""

    dbfile: pathlib.Path = pathlib.Path("ellclockin.db")

    def shvbroker_config(self) -> shv.broker.RpcBrokerConfig:
        """Provide new SHV Broker config based on this configuration."""
        bconf = shv.broker.RpcBrokerConfig(
            listen=[shv.RpcUrl.parse(f"tcp://localhost:{self.port}")],
            roles=[
                shv.broker.RpcBrokerConfig.Role(
                    "browse", access={shv.RpcMethodAccess.BROWSE: {"**:ls", "**:dir"}}
                ),
                shv.broker.RpcBrokerConfig.Role(
                    "clockin", access={shv.RpcMethodAccess.WRITE: {"clockin/**:*"}}
                ),
            ],
        )
        for name, shapass in self.users.items():
            bconf.users.add(
                bconf.User(
                    name=name,
                    password=shapass,
                    login_type=shv.RpcLoginType.SHA1,
                    roles=["clockin", "browse"],
                )
            )
        return bconf

    @classmethod
    def load(cls, path: pathlib.Path) -> ServerConfig:
        """Load configuration file."""
        config = configparser.ConfigParser()
        config.read(path)
        return cls(
            port=config.getint("server", "port", fallback=cls.port),
            users={name: config["users"][name] for name in config.options("users")},
            dbfile=pathlib.Path(config.get("db", "file", fallback=cls.dbfile)),
        )
