"""Implementation for the ellclockin server application entrypoint."""

import argparse
import asyncio
import logging

from .. import VERSION
from .server import Server, ServerConfig

logger = logging.getLogger(__name__)


def parse_args() -> argparse.Namespace:
    """Parse passed arguments and return result."""
    parser = argparse.ArgumentParser(
        prog="ellclockin-server", description="Server for ellclockin"
    )
    parser.add_argument("--version", action="version", version="%(prog)s " + VERSION)
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help="Increase verbosity level of logging",
    )
    parser.add_argument(
        "-q",
        action="count",
        default=0,
        help="Decrease verbosity level of logging",
    )
    parser.add_argument("config", nargs=1, help="Path to the configuration file.")
    return parser.parse_args()


async def async_main(config: ServerConfig) -> None:
    """Application's asynchronous entrypoint."""
    broker = Server(config)
    try:
        await broker.serve_forever()
    finally:
        await broker.terminate()


def main() -> None:
    """Application's entrypoint."""
    args = parse_args()

    log_levels = sorted(logging.getLevelNamesMapping().values())[1:]
    logging.basicConfig(
        level=log_levels[sorted([1 - args.v + args.q, 0, len(log_levels) - 1])[1]],
        format="[%(asctime)s] [%(levelname)s] - %(message)s",
    )

    asyncio.run(async_main(ServerConfig.load(args.config)))


if __name__ == "__main__":
    main()
