"""Implementation of command line application."""

import argparse
import logging

from . import VERSION

logger = logging.getLogger(__name__)


def parse_args() -> argparse.Namespace:
    """Parse passed arguments and return result."""
    parser = argparse.ArgumentParser(prog="ellclockin", description="Foo counter")
    parser.add_argument("--version", action="version", version="%(prog)s " + VERSION)
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help="Increase verbosity level of logging",
    )
    parser.add_argument(
        "-q",
        action="count",
        default=0,
        help="Decrease verbosity level of logging",
    )
    return parser.parse_args()


def main() -> None:
    """Application's entrypoint."""
    args = parse_args()

    log_levels = sorted(logging.getLevelNamesMapping().values())[1:]
    logging.basicConfig(
        level=log_levels[sorted([1 - args.v + args.q, 0, len(log_levels) - 1])[1]],
        format="[%(asctime)s] [%(levelname)s] - %(message)s",
    )

    # TODO continue with your code


if __name__ == "__main__":
    main()
